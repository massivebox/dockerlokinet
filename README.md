# Docker Lokinet Stuff

This repository contains various Dockerfiles for running Lokinet inside Docker together with some other program.  
In each folder is a different project:

| Name                                                         | Readme                                                       | Description                                             | Status  | Pre-Built image                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------- | ------- | ---------------------------------------------------------- |
| [lokinet](https://codeberg.org/massivebox/dockerlokinet/src/branch/main/lokinet) | [Here](https://codeberg.org/massivebox/dockerlokinet/src/branch/main/lokinet/README.md) | Base Lokinet image on which the others are built on     | Working | [Here](https://hub.docker.com/r/massiveboxe/lokinet)       |
| [caddy-lokinet](https://codeberg.org/massivebox/dockerlokinet/src/branch/main/caddy-lokinet) | [Here](https://codeberg.org/massivebox/dockerlokinet/src/branch/main/caddy-lokinet/README.md) | Caddy server running under Lokinet.                     | Working | [Here](https://hub.docker.com/r/massiveboxe/caddy-lokinet) |
| socks-lokinet                                                | WIP                                                          | Socks5 server that routes your traffic through Lokinet. | WIP     | WIP                                                        |
|                                                              |                                                              | More coming soon™️                                       |         |                                                            |

Pre-built images are not guaranteed to be updated.  
Everything you see here is beta software, that has not been extensively tested and has no guarantee of working. Please, if you find bugs, report them by opening an issue or in the support group.

## Support

Need help? You can join my open group on Session and ask there:  
`https://sogs.massivebox.eu.org/main?public_key=021ac6279595d141375ca49617e2605d8c09bb3a2e4e26fdc657572f70b80939`   
You can also contact me on Session at `massivebox`.

## Contribute

Feel free to contribute by opening issues, pull requests or reporting bugs in the support group.  
You can even send some $OXEN at `L7n8qURqkbSPraTADG8J6ujE8acs9crvZVzXTUacShWVSjLUEWYXCUTKuBvGTDuffDdNCR1iuGPT5RSoAyNgoBZuQov15fG`

## License

[![GPLv3 Logo.svg](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/GPLv3_Logo.svg/220px-GPLv3_Logo.svg.png)](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/GPLv3_Logo.svg/220px-GPLv3_Logo.svg.png)

```text
DockerLokinet - Collection of Dockerfiles to use software running Lokinet inside Docker.
Copyright (C) 2021  MassiveBox

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```